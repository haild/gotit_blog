TAG=$(shell git describe --tags --long)

build:
	pipenv lock -r > requirements.txt
	docker build --pull -t gotit_blog:$(TAG) .
	rm requirements.txt

push: build
	docker push gotit_blog:$(TAG)

run:
	docker run --name gotit_blog --rm -it -p 5000:5000 gotit_blog:$(TAG)

run-local:
	docker run -v $(PWD)/src/gotit_blog/local_config.py:/app/gotit_blog/local_config.py --name gotit_blog --rm -it -p 5000:5000 gotit_blog:$(TAG)

version:
	@echo $(TAG)
