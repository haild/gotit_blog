from flask import (
    Flask,
    flash,
    g,
    jsonify,
    redirect,
    render_template,
    request,
    session,
    url_for,
)
from flask.json import JSONEncoder
from flask_oauthlib.client import OAuth, OAuthException

import sqlalchemy as sa
import jwt
import bleach

from datetime import datetime, timedelta

import os
import re
import functools

from gotit_blog.models import db, User, BlogEntry, Like


class CustomJSONEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj.__class__, sa.ext.declarative.DeclarativeMeta):
            return obj.to_dict()
        return super(CustomJSONEncoder, self).default(obj)


app = Flask(__name__)
app.json_encoder = CustomJSONEncoder

app.config.setdefault('JWT_SECRET', '')
app.config.setdefault('SECRET_KEY', '')
app.config.setdefault('GOOGLE_CONSUMER_KEY', '')
app.config.setdefault('GOOGLE_CONSUMER_SECRET', '')
app.config.setdefault('FACEBOOK_CONSUMER_KEY', '')
app.config.setdefault('FACEBOOK_CONSUMER_SECRET', '')
app.config.setdefault('SQLALCHEMY_DATABASE_URI', 'sqlite:////tmp/test.db')

google = OAuth().remote_app(
    'google',
    base_url='https://www.googleapis.com/oauth2/v1/',
    authorize_url='https://accounts.google.com/o/oauth2/auth',
    request_token_url=None,
    request_token_params={
        'scope': 'https://www.googleapis.com/auth/userinfo.email',
    },
    access_token_url='https://accounts.google.com/o/oauth2/token',
    access_token_method='POST',
    app_key="GOOGLE"
)

facebook = OAuth().remote_app(
    'facebook',
    base_url='https://graph.facebook.com',
    authorize_url='https://www.facebook.com/dialog/oauth',
    request_token_url=None,
    request_token_params={'scope': 'email'},
    access_token_url='/oauth/access_token',
    access_token_method='GET',
    app_key="FACEBOOK"
)


@app.before_request
def before_request():
    # Extract user_id from JWT token (in case of APIs) or from session (in case
    # of web pages). Pretty hacky, should probably use blueprint instead.
    auth_header = request.headers.get('Authorization', '')
    if auth_header.startswith('Bearer '):
        token = auth_header[7:]
        secret = app.config['JWT_SECRET']
        try:
            user_id = jwt.decode(token, secret).get('user_id')
        except jwt.InvalidTokenError:
            user_id = None
    else:
        user_id = session.get('_id')

    if user_id:
        g.user = User.query.filter_by(id=user_id).first()
    else:
        g.user = None


def authenticated(f):
    @functools.wraps(f)
    def decorated_function(*args, **kwargs):
        if g.user is None:
            return jsonify(msg='This API require authentication'), 403
        if not g.user.has_onboarded():
            return jsonify(msg='You must onboard before taking any action'), 403

        return f(*args, **kwargs)
    return decorated_function


@app.route('/')
def index():
    if not g.user:
        return render_template('index.html.j2', user=g.user)
    if g.user.has_onboarded():
        exp = datetime.utcnow() + timedelta(days=1)
        token = jwt.encode({'user_id': g.user.id, 'exp': exp},
                           app.config['JWT_SECRET'])
        return render_template('index.html.j2', user=g.user, token=token.decode('ascii'))
    else:
        return render_template('onboard.html.j2', user=g.user)


@app.route('/login')
def login():
    src = request.args.get('src')
    if src == 'gg':
        provider = google
    elif src == 'fb':
        provider = facebook
    else:
        flash('Unsupported login provider')
        return redirect(url_for('index'))

    return provider.authorize(
        callback=url_for('oauth_authorized', src=src, _external=True),
        state=request.args.get('next') or request.referrer or None,
    )


@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))


@app.route('/<src>_authorized')
def oauth_authorized(src):
    """
    Endpoint acting as redirect URL from OAuth provider.

    For Google, the endpoint would be /gg_authorized. For Facebook, it's
    /fb_authorized. When the user arrives at this endpoint, two cases would
    happen:
    - The session is cleared, in which case the user would be
    logged-in/signed-up normally.
    - We're in the middle of a login merge, signified by the existance of
    merge_id and merge_src in session. The user is confirming they own the
    account owned by OAuth provider (e.g.  the user logged in via Facebook
    account and want to confirm they also own a Google account of the same
    email address).
    """
    if src == 'gg':
        provider = google
    elif src == 'fb':
        provider = facebook
    else:
        return 'Not Found', 404

    next_url = request.args.get('state') or url_for('index')

    try:
        resp = provider.authorized_response()
    except OAuthException:
        flash('Trouble validating OAuth response. Please try again.')
        return redirect(next_url)

    if resp is None:
        flash(u'You denied the request to sign in.')
        return redirect(next_url)

    if provider is google:
        me = google.get("userinfo", token=resp)
    else:
        me = facebook.get("me?fields=id,email", token=resp)

    if me.status != 200:
        flash('Trouble validating OAuth response. Please try again.')
        return redirect(next_url)

    email = me.data['email']
    user = User.query.filter_by(email=email).first()
    if not user:
        user = User(email=email)
        if provider is google:
            user.gg_id = me.data['id']
            user.src = 'gg'
        else:
            user.fb_id = me.data['id']
            user.src = 'fb'
        db.session.add(user)
        try:
            db.session.commit()
        except sa.exc.SQLAlchemyError:
            db.session.rollback()
            return 'Unknown error', 500
        else:
            session.clear()
            session['_id'] = user.id
            return redirect(next_url)

    elif getattr(user, src + '_id') == me.data['id']:
        if 'merge_id' in session:  # we're in the middle of a merge
            merge_src = session['merge_src']
            setattr(user, merge_src + '_id', session['merge_id'])
            try:
                db.session.commit()
            except sa.exc.SQLAlchemyError:
                db.session.rollback()
                return 'Unknown error', 500

        session.clear()
        session['_id'] = user.id
        return redirect(next_url)

    else:
        session.clear()
        session['merge_id'] = me.data['id']
        session['merge_src'] = src
        return render_template('login_merge.html.j2',
                               user=user, src=src, next=next_url)


@app.route('/onboard', methods=['POST'])
def do_onboard():
    if g.user.has_onboarded():
        return redirect(url_for('index'))
    if g.user.is_fb_user():
        username = sanitize_username(request.form.get('username'))
        phone = sanitize_phone(request.form.get('phone'))
        if not username or not phone:
            flash('Please fill in all required fields.')
            return render_template('onboard.html.j2', user=g.user), 400

        g.user.username = username
        g.user.phone = phone

    if g.user.is_gg_user():
        username = sanitize_username(request.form.get('username'))
        occupation = sanitize_occupation(get_occupation())
        if not username or not occupation:
            flash('Please fill in all required fields.')
            return render_template('onboard.html.j2', user=g.user), 400

        g.user.username = username
        g.user.occupation = occupation

    try:
        db.session.commit()
    except sa.exc.SQLAlchemyError:
        db.session.rollback()
        return 'Unknown error', 500
    else:
        return redirect(url_for('index'))


@app.route('/api/my/entries/', methods=['POST'])
@authenticated
def create_entries():
    payload = request.get_json()
    if not payload:
        return jsonify(msg='Malformed request'), 400

    subject = sanitize_subject(payload.get('subject', ''))
    content = bleach.clean(payload.get('content'))
    if not subject or not content:
        return jsonify(msg='Invalid parameter'), 400

    entry = BlogEntry(subject=subject, content=content, author_id=g.user.id)
    db.session.add(entry)
    try:
        db.session.commit()
    except sa.exc.SQLAlchemyError:
        db.session.rollback()
        return jsonify(msg='Unknown error'), 500
    else:
        return jsonify(entry)


@app.route('/api/my/entries/')
@authenticated
def list_my_entries():
    limit = min(int(request.args.get('limit', '10')), 100)
    entries = BlogEntry.query.filter_by(author_id=g.user.id).limit(limit).all()
    return jsonify(entries)


@app.route('/api/entries/')
@authenticated
def list_entries():
    limit = min(int(request.args.get('limit', '10')), 100)
    entries = BlogEntry.query.limit(limit).all()
    return jsonify(entries)


@app.route('/api/entries/<int:entry_id>')
@authenticated
def get_entry(entry_id):
    entry = BlogEntry.query.get(entry_id)
    if not entry:
        return jsonify(msg='Entry not found'), 404
    return jsonify(entry)


@app.route('/api/entries/<int:entry_id>/likes', methods=['POST'])
@authenticated
def create_entry_likes(entry_id):
    like = Like(user_id=g.user.id, blog_entry_id=entry_id)
    db.session.add(like)
    try:
        db.session.commit()
    except sa.exc.IntegrityError:
        # duplicate like, or non-existing ID
        db.session.rollback()
        return jsonify(msg='Invalid parameter'), 400
    else:
        return jsonify(like)


@app.route('/api/entries/<int:entry_id>/likes')
@authenticated
def get_entry_likes(entry_id):
    entry = BlogEntry.query.get(entry_id)
    if not entry:
        return jsonify(msg='Entry not found'), 404
    return jsonify(entry.to_dict(likes_limit=None)['liked_by'])


def get_occupation():
    """Get occupation from onboarding form."""
    occupation = request.form.get('occupation')
    if occupation not in ('student', 'teacher'):
        occupation = request.form.get('other_occupation')
    return occupation


# poor man's sanitizers
SUBJECT_REGEX = OCCUPATION_REGEX = USERNAME_REGEX = re.compile(r'^[\w\s]+$')

def sanitize_username(username):
    """Only accept Unicode alphanumeric characters and spaces for username.

    >>> print(sanitize_username('Luong Dong Hai'))
    Luong Dong Hai
    >>> print(sanitize_username('Lương Đông Hải'))
    Lương Đông Hải
    >>> print(sanitize_username('<script>alert(1)</script>'))
    None
    """
    return username if re.match(USERNAME_REGEX, username) else None


def sanitize_occupation(occupation):
    """
    Only accept Unicode alphanumeric characters and spaces for occupation.

    >>> print(sanitize_occupation('Student'))
    Student
    >>> print(sanitize_occupation("Rober'); DROP TABLE user; --"))
    None
    """
    return occupation if re.match(OCCUPATION_REGEX, occupation) else None


def sanitize_phone(phone):
    """
    Only accept digits for phone number.

    >>> print(sanitize_phone('0123456789'))
    0123456789
    >>> print(sanitize_phone('+123456789'))
    None
    """
    return phone if str.isdigit(phone) else None


def sanitize_subject(subject):
    """Only accept Unicode alphanumeric characters and spaces for subject.
    """
    return subject if re.match(SUBJECT_REGEX, subject) else None


app.config.from_mapping(os.environ)

try:
    from gotit_blog import local_config
    app.config.from_object(local_config)
except ImportError:
    pass

db.init_app(app)
with app.app_context():
    db.create_all()

if __name__ == '__main__':
    import doctest
    doctest.testmod()
    app.run(debug=1)
