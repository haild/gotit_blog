from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()


class Mixin(object):
    def to_dict(self):
        return {column.key: getattr(self, attr)
                for attr, column in self.__mapper__.c.items()}



class User(Mixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120), unique=True, nullable=False)
    username = db.Column(db.String(80), nullable=True)
    phone = db.Column(db.String(12), nullable=True)
    occupation = db.Column(db.String(120), nullable=True)
    gg_id = db.Column(db.String(32), nullable=True)
    fb_id = db.Column(db.String(32), nullable=True)
    src = db.Column(db.String(2), nullable=False)

    def __repr__(self):
        return '<User %r>' % self.username

    def is_fb_user(self):
        return self.src == 'fb'

    def is_gg_user(self):
        return self.src == 'gg'

    def has_onboarded(self):
        return (self.is_fb_user() and self.username is not None and self.phone is not None) or \
            (self.is_gg_user() and self.username is not None and self.occupation is not None)


class BlogEntry(Mixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    subject = db.Column(db.String(150), nullable=False)
    content = db.Column(db.Text, nullable=False)
    author_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

    author = db.relationship('User', backref=db.backref('blog_entries', lazy=True))

    def to_dict(self, likes_limit=2):
        res = super().to_dict()
        query = User.query.join(Like.user).join(Like.blog_entry).filter(BlogEntry.id==self.id)
        users = [r[0] for r in query.with_entities(User.username).limit(likes_limit)]
        count = query.count()
        res['liked_by'] = {"users": users, "count": count}
        return res


class Like(Mixin, db.Model):
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), primary_key=True, nullable=False)
    blog_entry_id = db.Column(db.Integer, db.ForeignKey('blog_entry.id'), primary_key=True, nullable=False)

    user = db.relationship('User', backref=db.backref('likes', lazy=True))
    blog_entry = db.relationship('BlogEntry', backref=db.backref('likes', lazy=True))
