SETUP OAUTH PROVIDERS
=====================

OAuth providers should be setup as follows:
1. Google: redirect URL must be `http://localhost:5000/gg_authorized`
2. Facebook: redirect URL must be `http://localhost:5000/fb_authorized`

INSTALL
=======

1. Install system dependencies

        apt-get update && apt-get install libmysqlclient-dev

2. Install application dependencies

        pip install -r requirements.txt

3. Prepare configuration: create a file at `src/gotit_blog/local_config.py` with content like this

        GOOGLE_CONSUMER_KEY = 'your_google_oauth_client_id'
        GOOGLE_CONSUMER_SECRET = 'your_google_oauth_client_secret'
        FACEBOOK_CONSUMER_KEY = 'your_facebook_oauth_client_id'
        FACEBOOK_CONSUMER_SECRET = 'your_facebook_oauth_client_secret'
        SECRET_KEY = 'a random string'
        JWT_SECRET = 'another random string'
        SQLALCHEMY_DATABASE_URI = 'mysql://mysql_user:mysql_password@mysql_host:mysql_port/mysql_db?charset=utf8mb4'

    If you have Docker installed, run this

        docker run --name gotit-mysql -e MYSQL_ROOT_PASSWORD=r00t -e MYSQL_DATABASE=gotit_blog -e MYSQL_USER=gotit -e MYSQL_PASSWORD='g0t!t' -it -p 3306:3306 mysql --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci

    and set `SQLALCHEMY_DATABASE_URI` to `mysql://gotit:g0t!t@172.17.0.1:3306/gotit_blog?charset=utf8mb4`

4. Run server

        cd src/
        env PYTHONPATH=. python -m gotit_blog.app

    If this fails but you have Docker installed, try running `make build run-local`

5. Open `http://localhost:5000` in web browser
6. All requirements in authentication flow could be tested via web UI. After successful onboarding, blog API access token is printed on screen.

API TESTING
===========

For ease of testing, you should install [HTTPie](https://httpie.org/)

    pip install [--user] httpie

- Create blog entry

        http POST localhost:5000/api/my/entries/ Authorization:"Bearer some-token" subject='some subject' content='some content'

- List personal blog entries

        http localhost:5000/api/my/entries/ Authorization:"Bearer some-token"

- List all blog entries

        http localhost:5000/api/entries/ Authorization:"Bearer some-token"

- Get a blog entry

        http localhost:5000/api/entries/some-id Authorization:"Bearer some-token"

- Like a blog entry

        http POST localhost:5000/api/entries/some-id/likes Authorization:"Bearer some-token"

- List a blog entry's likes

        http localhost:5000/api/entries/some-id/likes Authorization:"Bearer some-token"

