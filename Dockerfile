FROM python:3.6

WORKDIR /app
RUN apt-get update && apt-get install libmysqlclient-dev && rm -rf /var/lib/apt/lists/*
COPY requirements.txt conf/uwsgi.ini ./
RUN pip install --no-cache-dir -r requirements.txt
COPY src .

ENV NEW_RELIC_CONFIG_FILE /dev/null
ENV NEW_RELIC_LICENSE_KEY override_this_on_production
ENV NEW_RELIC_DEVELOPER_MODE 1
ENV NEW_RELIC_APP_NAME "GotIt Blog"

STOPSIGNAL QUIT
EXPOSE 5000

CMD ["newrelic-admin", "run-program", "uwsgi", "--ini=uwsgi.ini", "--http-socket=:5000"]
